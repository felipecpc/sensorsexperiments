package br.com.mbed.msensorexperiments;

import android.graphics.Color;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.github.mikephil.charting.formatter.FormattedStringCache;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class Graph implements Observer,OnChartValueSelectedListener {

    private LineChart mChart;
    private GraphProperties mGraphProperties;
    long mTimestamp=0;
    long mXValue=0;

    Stack<float[]> mValuesStack = new Stack();

    float mValues[];
    long mV;

    private int mVisibleXRangeMaximum = 120;
    private int bufferLimit = mVisibleXRangeMaximum*4;

    public Graph(LineChart lineChart, GraphProperties graphProperties){
        mChart=lineChart;
        mGraphProperties = graphProperties;
        initGraph();
    }

    public void onDataChanged(float[] values, long v) {
        mValuesStack.push(values);

        mV=v;
    }

    public void initGraph(){


        mChart.setDescription("");
        mChart.setNoDataTextDescription("No Data");


        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        mChart.setAutoScaleMinMaxEnabled(true);
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(mGraphProperties.getBackgroundColor());

        //No scale on wave amplitude
        mChart.setScaleYEnabled(false);
        //No highlights
        mChart.setHighlightPerDragEnabled(false);
        mChart.setHighlightPerTapEnabled(false);
        mChart.setHorizontalFadingEdgeEnabled(false);
        mChart.setVerticalFadingEdgeEnabled(false);
        mChart.setHardwareAccelerationEnabled(true);

        mChart.setDrawMarkerViews(false);

        XAxis xl = mChart.getXAxis();
        xl.setAvoidFirstLastClipping(true);
        xl.disableGridDashedLine();
        xl.setDrawLimitLinesBehindData(false);
        xl.setDrawAxisLine(false);


        xl.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
        xl.setGranularity(200L);
        xl.setTextColor(Color.rgb(255, 192, 56));
        xl.setValueFormatter(new AxisValueFormatter() {

            private FormattedStringCache.Generic<Long, Date> mFormattedStringCache = new FormattedStringCache.Generic<>(new SimpleDateFormat("HH:mm:ss"));

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Long v = (long) (value * 50)+mTimestamp;
                return mFormattedStringCache.getFormattedValue(new Date(v), v);
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });



        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.disableGridDashedLine();
        leftAxis.setEnabled(false);
        leftAxis.setDrawAxisLine(false);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.disableGridDashedLine();
        rightAxis.setDrawAxisLine(false);

        //Legend
        Legend l = mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);



        mValues = new float[mGraphProperties.getDatasetCount()];
        for(int x=0;x<mValues.length;x++)
            mValues[x]=0;

        mValuesStack.push(mValues);

        ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(3);
        exec.scheduleAtFixedRate(new DrawingTask(), 0, 50, TimeUnit.MILLISECONDS);

    }

    private void addValue(float[] values, long v) {




        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            LineDataSet set;
            for(int x=0;x<mGraphProperties.getDatasetCount();x++){
                set = (LineDataSet)mChart.getData().getDataSetByIndex(x);

                if(mChart.getData().getEntryCount()>=(bufferLimit)) {
                    set.removeFirst();
                }

                set.addEntry(new Entry(mXValue, values[x]));
            }

            mXValue++;
            mChart.notifyDataSetChanged();

            mChart.setVisibleXRangeMaximum(mVisibleXRangeMaximum);
            mChart.moveViewToX(mXValue);


        } else {

            mTimestamp=v;

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();

            for(int x=0;x<mGraphProperties.getDatasetCount();x++){
                dataSets.add(createSet(x));
            }

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            // set data
            mChart.setData(data);
        }
    }


    private LineDataSet createSet(int index) {

        LineDataSet set = new LineDataSet(null, mGraphProperties.getDataSetLabel(index));
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(mGraphProperties.getDatasetColor(index));
        set.setCircleColor(mGraphProperties.getDatasetColor(index));
        set.setLineWidth(2f);
        set.setCircleRadius(1f);
        set.setFillAlpha(0);
        set.setDrawCircles(false);
        set.setFillColor(mGraphProperties.getDatasetColor(index));
        set.setValueTextColor(mGraphProperties.getDatasetColor(index));
        set.setDrawValues(false);
        return set;
    }


    @Override
    public void update(Observable observable, Object data) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    class DrawingTask implements Runnable {



        @Override
        public void run() {
            if(mValuesStack.size()!=0)
                mValues=mValuesStack.pop();

            addValue(mValues,mV);
        }
    }

}
