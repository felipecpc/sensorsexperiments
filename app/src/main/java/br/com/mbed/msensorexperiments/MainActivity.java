package br.com.mbed.msensorexperiments;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private Graph mGraph;
    private LineChart mChart;

    private Graph mGraph2;
    private LineChart mChart2;

    private SensorManager mSensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mChart = (LineChart) findViewById(R.id.mychart);
        GraphProperties lProperties = new GraphProperties();
        lProperties.setDatasetCount(3);
        String lLabels[]={"Accelerometer X","Y","Z"};
        lProperties.setDataSetLabel(lLabels);
        mGraph = new Graph(mChart, lProperties);

        mChart2 = (LineChart) findViewById(R.id.mychart1);
        GraphProperties lProperties2 = new GraphProperties();
        lProperties2.setDatasetCount(3);
        String lLabels2[]={"Magnetometer X","Y","Z"};
        lProperties2.setDataSetLabel(lLabels2);
        mGraph2 = new Graph(mChart2, lProperties2);


        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            mGraph.onDataChanged(event.values, System.currentTimeMillis());
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {

            mGraph2.onDataChanged(event.values, System.currentTimeMillis());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and
        // accelerometer sensors
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
